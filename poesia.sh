#!/bin/bash

echo -e "\e[1;31mRetrato - Cecília Meireles\e[0m"
sleep 1
echo -e "\e[1;34mEu não tinha este rosto de hoje \e[0m"
sleep 1
echo -e  "\e[1;32mAssim calmo, assim triste, assim magro, \e[0m"
sleep 1
echo -e "\e[1;33mNem estes olhos tão vazios, \e[0m"
sleep 1
echo -e "\e[1;35mNem o lábio amargo. \e[0m"
echo " "
sleep 1
echo -e "\e[1;36mEu não tinha estas mãos sem força, \e[0m"
sleep 1
echo -e "\e[1;37mTão paradas e frias e mortas; \e[0m"
sleep 1
echo -e "\e[1;34mEu não tinha este coração \e[0m"
sleep 1
echo -e "\e[1;32mQue nem se mostra. \e[0m"
echo " "
sleep 1
echo -e "\e[1;33mEu não dei por esta mudança, \e[0m"
sleep 1
echo -e "\e[1;35mTão simples, tão certa, tão fácil: \e[0m"
sleep 1
echo -e "\e[1;36m - Em que espelho ficou perdida \e[0m"
sleep 1
echo -e "\e[1;37ma minha face? \e[0m"
